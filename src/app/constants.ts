export class Constants {

    public static MODE = 'main';

    public static BMI_URL = '/basic-page-initilization';
    public static MARKERS_URL = '/markers';
    public static ANIMATIONS_URL = '/animations';
    public static MAP_INTERACTION_URL = '/map-interaction';
    public static ROUTING_URL = '/routing';
    public static SEARCH_POLYGON_URL = '/search-polygon';

}
