import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicMapInitializationComponent } from './components/basic-map-initialization/basic-map-initialization.component';
import { MaterialImportsModule } from './Material-import/material-imports.module';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { MarkersComponent } from './components/markers/markers.component';
import { AnimationsComponent } from './components/animations/animations.component';
import { MapInteractionComponent } from './components/map-interaction/map-interaction.component';
import { RoutingComponent } from './components/routing/routing.component';
import { RouterModule } from '@angular/router';
import { SearchPolygonComponent } from './components/search-polygon/search-polygon.component';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    BasicMapInitializationComponent,
    MarkersComponent,
    AnimationsComponent,
    MapInteractionComponent,
    RoutingComponent,
    SearchPolygonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialImportsModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
