import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimationsComponent } from './components/animations/animations.component';
import { BasicMapInitializationComponent } from './components/basic-map-initialization/basic-map-initialization.component';
import { MapInteractionComponent } from './components/map-interaction/map-interaction.component';
import { MarkersComponent } from './components/markers/markers.component';
import { RoutingComponent } from './components/routing/routing.component';
import { SearchPolygonComponent } from './components/search-polygon/search-polygon.component';

const routes: Routes = [
  {path: "", component: BasicMapInitializationComponent},
  {path: "basic-page-initilization", component: BasicMapInitializationComponent},
  {path: "markers", component: MarkersComponent},
  {path: "animations", component: AnimationsComponent},
  {path: "map-interaction", component: MapInteractionComponent},
  {path: "routing", component: RoutingComponent},
  {path: "search-polygon", component: SearchPolygonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
