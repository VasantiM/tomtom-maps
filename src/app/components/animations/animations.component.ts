import { Component, OnInit } from '@angular/core';
import tt from '@tomtom-international/web-sdk-maps';
import { Observable, Subscriber } from 'rxjs';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-animations',
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.scss','../../../assets/maps.css', '../../../assets/css/index.css']
})
export class AnimationsComponent implements OnInit {
  animationmap:any = undefined;
  marker: any;
  popup: any;
  constants = Constants;

  tomtomKey = environment.tomtomKey;
  styleUrl = '';

  constructor() { }

  ngOnInit(): void {
  }

  public ngAfterViewInit(): void {
    this.loadMap();
  }

 
  private loadMap(): void {
    this.animationmap = tt.map({
      key: this.tomtomKey,
      pitch: 60,
      zoom: 16,
      center: [21.00462, 52.23164],
      container: 'animationmap',
      style: this.getUrl()
    });

    this.animationmap.addControl(new tt.FullscreenControl());
    this.animationmap.addControl(new tt.NavigationControl(), 'bottom-right');

    this.animationmap.on('load', () => {
      var layers = this.animationmap.getStyle().layers;
      for (var i = 0; i < layers.length; i++) {
          var layer = layers[i];
          if (layer.type === 'symbol' && layer['source-layer'] === 'Point of Interest') {
              this.animationmap.removeLayer(layer.id);
          }
      }
      requestAnimationFrame(() => this.rotateCamera());
    });
  
  }

  rotateCamera() {
    var rotationDegree = performance.now() / 100 % 360;
    this.animationmap.rotateTo(rotationDegree, { duration: 0 });
    requestAnimationFrame(() => this.rotateCamera());
  }

  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + Constants.MODE +
    '&poi=poi_main';
  }
  changeTheme(){
    Constants.MODE = Constants.MODE=='main' ? 'night' : 'main';
    this.animationmap.setStyle(this.getUrl());
    
  
  }
}
