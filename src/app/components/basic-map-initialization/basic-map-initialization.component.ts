import { Component, OnInit } from '@angular/core';
import '../../../assets/js/mobile-or-tablet';
import '../../../assets/js/foldable';
import tt from '@tomtom-international/web-sdk-maps';
import { environment } from 'src/environments/environment';
import { Constants } from 'src/app/constants';

declare var isMobileOrTablet: any;
declare var Foldable: any;

@Component({
  selector: 'app-basic-map-initialization',
  templateUrl: './basic-map-initialization.component.html',
  styleUrls: ['./basic-map-initialization.component.scss', '../../../assets/maps.css', '../../../assets/css/index.css']
})
export class BasicMapInitializationComponent implements OnInit {
  bmimap: any = undefined;
  tomTomKey = environment.tomtomKey;
  styleUrl = '';
  constants = Constants;
  constructor() { }

  ngOnInit(): void {
    var initMapCenter: tt.LngLatLike = [8.6753, 9.0820];
    this.bmimap = tt.map({
        key: this.tomTomKey,
        container: 'bmimap',
        style: this.getUrl(),
        zoom: 0,
        center: initMapCenter,
        dragPan: !isMobileOrTablet()
    });

    this.bmimap.addControl(new tt.FullscreenControl());
    this.bmimap.addControl(new tt.NavigationControl(), 'bottom-right');

  }


  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + Constants.MODE +
    '&poi=poi_main';
  }
  changeTheme(){
    Constants.MODE = Constants.MODE=='main' ? 'night' : 'main';
    this.bmimap.setStyle(this.getUrl());
    
  
  }
}
