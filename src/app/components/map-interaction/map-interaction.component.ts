import { Component, OnInit } from '@angular/core';
import '../../../assets/js/mobile-or-tablet';
import '../../../assets/js/foldable';
import tt from '@tomtom-international/web-sdk-maps';
import { environment } from 'src/environments/environment';
import { Constants } from 'src/app/constants';

declare var isMobileOrTablet: any;


@Component({
  selector: 'app-map-interaction',
  templateUrl: './map-interaction.component.html',
  styleUrls: ['./map-interaction.component.scss','../../../assets/maps.css', '../../../assets/css/index.css']
})
export class MapInteractionComponent implements OnInit {
  mimap: any = undefined;
  tomTomKey = environment.tomtomKey;
  styleUrl = '';
  constants = Constants;
  constructor() { }

  ngOnInit(): void {
    var initMapCenter: tt.LngLatLike = [8.6753, 9.0820];
    this.mimap = tt.map({
        key: this.tomTomKey,
        container: 'mimap',
        style: this.getUrl(),
        center: [2.29499, 48.87333],
        pitch: 60,
        bearing: 95,
        zoom: 17,
        dragPan: !isMobileOrTablet()
    });

    this.mimap.addControl(new tt.FullscreenControl());
    this.mimap.addControl(new tt.NavigationControl({
      showPitch: true,
      showExtendedRotationControls: true,
      showExtendedPitchControls: true,
  }), 'bottom-right');

  }


  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + Constants.MODE +
    '&poi=poi_main';
  }
  changeTheme(){
    Constants.MODE = Constants.MODE=='main' ? 'night' : 'main';
    this.mimap.setStyle(this.getUrl());
    
  
  }
}
