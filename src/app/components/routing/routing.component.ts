import { Component, OnInit } from '@angular/core';
import '../../../assets/js/mobile-or-tablet';
import '../../../assets/js/searchbox-enter-submit';
import { services } from '@tomtom-international/web-sdk-services';
import SearchBox from '@tomtom-international/web-sdk-plugin-searchbox';
import tt from '@tomtom-international/web-sdk-maps';
import { environment } from 'src/environments/environment';
import * as $ from "jquery";
import { Constants } from 'src/app/constants';

declare var isMobileOrTablet: any;
declare var handleEnterSubmit: any;

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss', '../../../assets/SearchBox.css', '../../../assets/maps.css','../../../assets/css/index.css',
              '../../../assets/css/icons-css/routing.css']
})
export class RoutingComponent implements OnInit {
  routingmap: any = undefined;
  tomTomKey = environment.tomtomKey;
  finishMarkerElement:any;
  startMarkerElement:any;
  routeMarkers: any = [];
  routePoints: any = [];
  searchBox: any = {start: SearchBox, finish: SearchBox};
  styleUrl = '';
  theme = Constants.MODE;
  constants = Constants;
  constructor() { }

  ngOnInit(): void {
    var initMapCenter: tt.LngLatLike = [8.6753, 9.0820];
    this.routingmap = tt.map({
        key: this.tomTomKey,
        container: 'routingmap',
        style: this.getUrl(),
        zoom: 0,
        center: initMapCenter,
        dragPan: !isMobileOrTablet()
    });

    this.finishMarkerElement = this.createMarkerElement('finish');
    this.startMarkerElement = this.createMarkerElement('start');
  
    this.routingmap.addControl(new tt.FullscreenControl());
    this.routingmap.addControl(new tt.NavigationControl(), 'bottom-right');

    this.routingmap.on('load', () => {
      this.searchBox.start = this.createSearchBox('start');
      this.searchBox.finish = this.createSearchBox('finish');
    });

  }

  addRouteMarkers(type: any, point: any) {
    var lngLat = point && point[type + 'Point'] || this.routePoints[type];

    if (!this.routeMarkers[type] && this.routePoints[type]) {
        this.routeMarkers[type] = this.createMarker(type, lngLat);
    }
    if (this.routeMarkers[type]) {
        this.routeMarkers[type].setLngLat(this.routePoints[type]);
    }
  }
  clearMap() {
    if (!this.routingmap.getLayer('route')) {
        return;
    }
    this.routingmap.removeLayer('route');
    this.routingmap.removeSource('route');
  }
  centerMap(lngLat: any) {
    this.routingmap.flyTo({
        center: lngLat,
        speed: 10,
        zoom: 8
    });
  }
  createMarker(type: any, lngLat: any) {
    var markerElement = type === 'start' ? this.startMarkerElement : this.finishMarkerElement;

    return new tt.Marker({ draggable: true, element: markerElement })
        .setLngLat(lngLat)
        .addTo(this.routingmap)
        .on('dragend', () => {this.getDraggedMarkerPosition(type)});
  } 
  createMarkerElement(type: any) {
    var element = document.createElement('div');
    var innerElement = document.createElement('div');

    element.className = 'draggable-marker';
    innerElement.className = 'tt-icon -white -' + type;
    element.appendChild(innerElement);
    return element;
  }
  createSearchBox(type: any) {
    var searchBox = new SearchBox(services, {
        showSearchButton: false,
        searchOptions: {
            key: this.tomTomKey
        },
        labels: {
            placeholder: 'Query e.g. Washington'
        }
    });

    $('#' + type + 'SearchBox').append(searchBox.getSearchBoxHTML());
    searchBox.on('tomtom.searchbox.resultscleared', this.onResultCleared.bind(null, type));

    searchBox.on('tomtom.searchbox.resultsfound', (event) => {
        handleEnterSubmit(event, this.onResultSelected.bind(this), type);
    });

    searchBox.on('tomtom.searchbox.resultselected', (event) => {
        if (event.data && event.data.result) {
            this.onResultSelected(event.data.result, type);
        }
    });

    return searchBox;
  }
  getDraggedMarkerPosition(type:any) {
    var lngLat = this.routeMarkers[type].getLngLat();

    this.performReverseGeocodeRequest(lngLat)
        .then((response: any) => {
            var address = response.addresses[0];
            var freeFormAddress = address.address.freeformAddress;

            if (!freeFormAddress) {
                this.clearMap();
                alert('Address not found, please choose a different place');
                return;
            }
            this.searchBox[type]
                .getSearchBoxHTML()
                .querySelector('input.tt-search-box-input')
                .value = freeFormAddress;
            var position = {
                lng: address.position.lng,
                lat: address.position.lat
            };

            this.updateMapView(type, position);
        });
  }
  handleDrawRoute(type: any) {
    this.performCalculateRouteRequest()
    .then((response) => {
        var geojson = response.toGeoJson();
        var coordinates = geojson.features[0].geometry.coordinates;

        this.clearMap();
        this.routingmap.addLayer({
            'id': 'route',
            'type': 'line',
            'source': {
                'type': 'geojson',
                'data': geojson
            },
            'paint': {
                'line-color': '#4a90e2',
                'line-width': 6
            }
        });
        var bounds = new tt.LngLatBounds();
        var point = {
            startPoint: coordinates[0],
            finishPoint: coordinates.slice(-1)[0]
        };

        this.addRouteMarkers(type, point);
        coordinates.forEach((point) => {
            bounds.extend(tt.LngLat.convert([+point[0], +point[1]]));
        });
        this.routingmap.fitBounds(bounds, { duration: 0, padding: 50 });
    }).catch(() => {
        this.clearMap();
        console.error('There was a problem calculating the route');
    });
  }
  onResultCleared(type:any) {
    this.routePoints[type] = null;
    if (this.routeMarkers[type]) {
        this.routeMarkers[type].remove();
        this.routeMarkers[type] = null;
    }
    if (this.routePoints.start || this.routePoints.finish) {
        var lngLat = type === 'start' ? this.routePoints.finish : this.routePoints.start;

        this.clearMap();
        this.centerMap(lngLat);
    }
  }
  onResultSelected(result: any, type: any) {
    var position = result.position;

    this.updateMapView(type, position);
  }
  performCalculateRouteRequest() {
    return services.calculateRoute({
        key: this.tomTomKey,
        traffic: false,
        locations: this.routePoints.start.join() + ':' + this.routePoints.finish.join()
    });
  }
  performReverseGeocodeRequest(lngLat: any) {
    return services.reverseGeocode({
        key: this.tomTomKey,
        position: lngLat
    });
  }
  updateMapView(type: any, position:any) {
    this.routePoints[type] = [position.lng, position.lat];
    if (this.routePoints.start && this.routePoints.finish) {
        this.handleDrawRoute(type);
    } else {
        this.addRouteMarkers(type, '');
        this.centerMap(this.routePoints[type]);
    }
  }
  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + Constants.MODE +
    '&poi=poi_main';
  }
  changeTheme(){
    Constants.MODE = Constants.MODE=='main' ? 'night' : 'main';
    this.routingmap.setStyle(this.getUrl());
    
  
  }
}
