import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Constants } from 'src/app/constants';
import { RoutingComponent } from '../routing/routing.component';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  options = {
    bottom: 0,
    fixed: false,
    top: 0,
  };

  constants = Constants;
  theme = Constants.MODE;
  constructor() {}

  ngOnInit(): void {
  }

  changeTheme(){
    this.theme = this.theme=='main' ? 'night' : 'main';
    Constants.MODE = this.theme;
  }

  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + this.theme +
    '&poi=poi_main';
  }

}
