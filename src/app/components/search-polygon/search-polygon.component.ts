import { AfterViewInit, Component, OnInit } from '@angular/core';
import '../../../assets/js/mobile-or-tablet';
import '../../../assets/js/searchbox-enter-submit';
import { services } from '@tomtom-international/web-sdk-services';
import SearchBox from '@tomtom-international/web-sdk-plugin-searchbox';
import tt from '@tomtom-international/web-sdk-maps';
import { environment } from 'src/environments/environment';
import * as $ from "jquery";
import { Constants } from 'src/app/constants';

declare var isMobileOrTablet: any;
declare var handleEnterSubmit: any;


@Component({
  selector: 'app-search-polygon',
  templateUrl: './search-polygon.component.html',
  styleUrls: ['./search-polygon.component.scss', '../../../assets/SearchBox.css', '../../../assets/maps.css', '../../../assets/css/index.css',
    '../../../assets/css/icons-css/routing.css']
})
export class SearchPolygonComponent implements OnInit, AfterViewInit {

  constants = Constants;
  polygonmap: any = undefined;
  popup: any = undefined;
  tomtomKey = environment.tomtomKey;
  searchBox: any = { start: SearchBox, finish: SearchBox };
  styleUrl = '';
  theme = Constants.MODE;
  mapZoom = 0;

  NO_POLYGON_MESSAGE = 'For the given result there is no polygon attached.';
  POLYGON_ID = 'searchResultPolygon';
  OUTLINE_ID = 'searchResultOutline';

  constructor() { }

  ngOnInit(): void {
  }

  public ngAfterViewInit(): void {
    this.loadMap();
  }

  private loadMap(): void {
    this.polygonmap = tt.map({
      key: this.tomtomKey,
      container: 'polygonmap',
      zoom: 0,
      dragPan: !isMobileOrTablet()
    });
    this.polygonmap.addControl(new tt.FullscreenControl());
    this.polygonmap.addControl(new tt.NavigationControl());

    this.polygonmap.on('zoomend', function () {
      this.mapZoom = Number(this.polygonmap.getZoom()).toFixed(2);
    });

    this.popup = new tt.Popup({ className: 'tt-popup', closeOnClick: false });
    // showStartSearchingPopup();



  

    // 

 

    

    //


    var ttSearchBox = new SearchBox(services, {
      searchOptions: {
        key: this.tomtomKey,
        language: 'en-GB'
      },
      labels: {
        noResultsMessage: 'No results with a polygon found',
        placeholder: 'Query e.g. Washington'
      }
    });
    var searchBoxPlaceholder = document.getElementById('searchBoxPlaceholder') as HTMLElement; 
    searchBoxPlaceholder.appendChild(ttSearchBox.getSearchBoxHTML());

    ttSearchBox.on('tomtom.searchbox.resultselected', (event) => {
      this.loadPolygon(event.data.result);
    });

    ttSearchBox.on('tomtom.searchbox.resultsfound', (event) => {
      handleEnterSubmit(event, this.loadPolygon.bind(this));
    });

    ttSearchBox.on('tomtom.searchbox.resultscleared', () => {
      this.clearLayer(this.POLYGON_ID);
      this.clearLayer(this.OUTLINE_ID);
      // showStartSearchingPopup();
    });
  }
  showStartSearchingPopup() {
      this.popup.setLngLat(this.polygonmap.getCenter())
        .setHTML('<strong>Start searching.</strong>');
      if (!this.popup.isOpen()) {
        this.popup.addTo(this.polygonmap);
      }
    }

  clearLayer(layerID) {
    if (this.polygonmap.getLayer(layerID)) {
      this.polygonmap.removeLayer(layerID);
      this.polygonmap.removeSource(layerID);
    }
  }
  showLoadingPopup() {
    this.popup.setHTML('<strong>Loading...</strong>');
    if (!this.popup.isOpen()) {
      this.popup.setLngLat(this.polygonmap.getCenter());
      this.popup.addTo(this.polygonmap);
    }
  }

  renderPolygon(searchResult, additionalDataResult) {
      var geoJson = additionalDataResult && additionalDataResult.geometryData;
      if (!geoJson) {
        throw Error(this.NO_POLYGON_MESSAGE);
      }

      this.polygonmap.addLayer({
        id: this.POLYGON_ID,
        type: 'fill',
        source: {
          type: 'geojson',
          data: geoJson
        },
        paint: {
          'fill-color': '#004B7F',
          'fill-opacity': 0.5
        }
      });

      this.polygonmap.addLayer({
        id: this.OUTLINE_ID,
        type: 'line',
        source: {
          type: 'geojson',
          data: geoJson
        },
        paint: {
          'line-color': '#004B7F',
          'line-width': 1
        }
      });

      this.polygonmap.flyTo({
        center:{lat:geoJson.features[0].geometry.coordinates[0][0][1],lng:geoJson.features[0].geometry.coordinates[0][0][0]},
        zoom:10
      });

      var boundingBox = searchResult.boundingBox || searchResult.viewport;
      // boundingBox = new tt.LngLatBounds([
      //   [boundingBox.topLeftPoint.lng, boundingBox.btmRightPoint.lat],
      //   [boundingBox.btmRightPoint.lng, boundingBox.topLeftPoint.lat]
      // ]);

      this.polygonmap.fitBounds(boundingBox, { padding: 100, linear: true });
    }
   showPopup(searchResult) {
      var resultName = searchResult.address && searchResult.address.freeformAddress;
      if (!searchResult.position) {
        return;
      }

      var resultPosition = {
        lng: searchResult.position.lng,
        lat: searchResult.position.lat
      };

      var popupResultName = '<strong>' + resultName + '</strong>';
      var popupLatLon = '<div>' + resultPosition.lat + ', ' + resultPosition.lng + '</div>';
      this.popup.setHTML('<div>' + popupResultName + popupLatLon + '</div>');
      this.popup.setLngLat([resultPosition.lng, resultPosition.lat]);
      this.popup.addTo(this.polygonmap);
    }
  loadPolygon(searchResult) {
    if (!searchResult) {
      return;
    }

    return new Promise<void>((resolve) => {
      this.clearLayer(this.POLYGON_ID);
      this.clearLayer(this.OUTLINE_ID);
      this.showLoadingPopup();
      resolve();
    }).then(() => {
      var polygonId = searchResult && searchResult.dataSources && searchResult.dataSources.geometry.id;
      if (!polygonId) {
        throw Error(this.NO_POLYGON_MESSAGE);
      }

      return services.additionalData({
        key: this.tomtomKey,
        geometries: [polygonId],
      });
    }).then((additionalDataResponse) => {
      console.log(additionalDataResponse)
      var additionalDataResult = additionalDataResponse && additionalDataResponse['additionalData'] &&
        additionalDataResponse['additionalData'][0];
      this.renderPolygon(searchResult, additionalDataResult);
      this.showPopup(searchResult);
    }).catch((error) => {
      this.clearPopup();
    });
  }
  clearPopup() {
    this.popup.remove();
  }
  getUrl() {

    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
      '?map=' + 'basic' + '_' + Constants.MODE +
      '&poi=poi_main';
  }
  changeTheme() {
    Constants.MODE = Constants.MODE == 'main' ? 'night' : 'main';
    // this.markermap.setStyle(this.getUrl());


  }
}
