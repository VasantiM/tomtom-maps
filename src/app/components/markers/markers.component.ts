import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import tt from '@tomtom-international/web-sdk-maps';
import '../../../assets/js/mobile-or-tablet';
 
declare var isMobileOrTablet: any;

@Component({
  selector: 'app-markers',
  templateUrl: './markers.component.html',
  styleUrls: ['./markers.component.scss','../../../assets/maps.css', '../../../assets/css/index.css']
})
export class MarkersComponent implements OnInit, AfterViewInit {

  markermap:any = undefined;
  marker: any;
  popup: any;
  markerElement:any;
  markerContentElement: any;
  iconElement:any;

  tomtomKey = environment.tomtomKey;
  styleUrl = '';
  constants = Constants;

  constructor() { }

  ngOnInit(): void {
  }

  public ngAfterViewInit(): void {
    this.loadMap();
  }

  private getCurrentPosition(): any {
    return new Observable((observer: Subscriber<any>) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position: any) => {
          observer.next({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
          observer.complete();
        });
      } else {
        observer.error();
      }
    });
  }

  private loadMap(): void {
    this.markermap = tt.map({
      key: this.tomtomKey,
      container: 'markermap',
      style: this.getUrl(),
      dragPan: !isMobileOrTablet()
    });

    this.markermap.addControl(new tt.FullscreenControl());
    this.markermap.addControl(new tt.NavigationControl(), 'bottom-right');

    this.getCurrentPosition()
    .subscribe((position: any) => {
      this.markermap.flyTo({
        center: {
          lat: position.latitude,
          lng: position.longitude,
        },
        zoom: 13,
      });

      this.popup = new tt.Popup({ anchor: 'bottom', offset: { bottom: [0, -40] } }).setHTML('Angular TomTom');

      this.markerElement = document.createElement('div');
      this.markerElement.className = 'marker';

      this.markerContentElement = document.createElement('div');
      this.markerContentElement.className = 'marker-content';
      this.markerContentElement.style.backgroundColor = '#5327c3';
      this.markerElement.appendChild(this.markerContentElement);

      this.iconElement = document.createElement('div');
      this.iconElement.className = 'marker-icon';
      this.iconElement.style.backgroundImage =
                'url(https://api.tomtom.com/maps-sdk-for-web/cdn/static/accident.colors-white.svg)';
      this.markerContentElement.appendChild(this.iconElement);

      this.marker = new tt.Marker({element: this.markerElement, anchor: 'bottom',
        draggable: true
      }).setLngLat({
        lat: position.latitude,
        lng: position.longitude,
      }).addTo(this.markermap);
      this.marker.setPopup(this.popup).togglePopup();

      this.marker.on('dragend', () => {
        var lngLat = this.marker.getLngLat();
        lngLat = new tt.LngLat(lngLat.lng, lngLat.lat);
    
        this.popup.setHTML(lngLat.toString());
        this.popup.setLngLat(lngLat);
        this.marker.setPopup(this.popup);
        this.marker.togglePopup();
      });

    });
  }

  getUrl(){
    
    return 'https://api.tomtom.com/style/1/style/21.1.0-*/' +
    '?map=' + 'basic' + '_' + Constants.MODE +
    '&poi=poi_main';
  }
  changeTheme(){
    Constants.MODE = Constants.MODE=='main' ? 'night' : 'main';
    this.markermap.setStyle(this.getUrl());
    
  
  }
}
